package com.msvc.calificacion.services;

import com.msvc.calificacion.schemas.Calificacion;

import java.util.List;

public interface CalifacionService {
    Calificacion create(Calificacion calificacion);

    List<Calificacion> getCalificaciones();

    List<Calificacion> getCalificacionesByUsuarioId(String id);

    List<Calificacion> getCalificacionesByHotelId(String id);
}
