package com.msvc.calificacion.services.impl;

import com.msvc.calificacion.repository.CalificacionRepository;
import com.msvc.calificacion.schemas.Calificacion;
import com.msvc.calificacion.services.CalifacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CalificacionServiceImpl implements CalifacionService {

    @Autowired
    private CalificacionRepository calificacionRepository;


    @Override
    public Calificacion create(Calificacion calificacion) {
        return calificacionRepository.save(calificacion);
    }

    @Override
    public List<Calificacion> getCalificaciones() {
        return calificacionRepository.findAll();
    }

    @Override
    public List<Calificacion> getCalificacionesByUsuarioId(String id) {
        return calificacionRepository.findByUsuarioId(id);
    }

    @Override
    public List<Calificacion> getCalificacionesByHotelId(String id) {
        return calificacionRepository.findByHotelId(id);
    }
}
