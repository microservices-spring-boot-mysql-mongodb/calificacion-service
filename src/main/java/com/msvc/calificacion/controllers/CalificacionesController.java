package com.msvc.calificacion.controllers;

import com.msvc.calificacion.schemas.Calificacion;
import com.msvc.calificacion.services.CalifacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/calificaciones")
public class CalificacionesController {

    @Autowired
    private CalifacionService califacionService;

    @PostMapping
    public ResponseEntity<Calificacion> save(@RequestBody Calificacion body) {

        Calificacion calif = califacionService.create(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(calif);
    }

    @GetMapping
    public ResponseEntity<List<Calificacion>> getAll() {
        return ResponseEntity.ok(califacionService.getCalificaciones());
    }

    @GetMapping("/hotel/{id}")
    public ResponseEntity<List<Calificacion>> getAllByHotel(@PathVariable String id) {
        return ResponseEntity.ok(califacionService.getCalificacionesByHotelId(id));
    }

    @GetMapping("/usuario/{id}")
    public ResponseEntity<List<Calificacion>> getAllByUsuario(@PathVariable String id) {
        return ResponseEntity.ok(califacionService.getCalificacionesByUsuarioId(id));
    }
}
